<?php

namespace YiiLogEx\Target;

use Yii;
use yii\log\Target;
use YiiLogEx\Adapter\AdapterInterface;
use YiiLogEx\Adapter\SmsV1;
use YiiLogEx\Exception\Exception;

/**
 * Class Sms
 * @package YiiLogEx\Target
 */
class Sms extends Target
{

    public $logVars = [];

    /** @var int  */
    public $messageLimit = 1000;    // chars

    /** @var array  */
    public $recipients = [];

    /** @var  AdapterInterface */
    protected $adapter = SmsV1::class;

    /**
     * @throws Exception
     */
    public function init()
    {
        parent::init();
        
        $this->messageLimit = $this->messageLimit > 0
            ? $this->messageLimit
            : 1000;

        if (is_string($this->adapter) && class_exists($this->adapter)) {
            $this->adapter = new $this->adapter();
            $this->adapter->setOptions([
                'message_limit' => $this->messageLimit,
            ]);
        }
        if (!$this->adapter instanceof AdapterInterface) {
             throw new Exception();
        }
    }

    /**
     *
     */
    public function export()
    {
        $message = implode("\n", array_map([$this, 'formatMessage'], $this->messages)) . "\n";
        $this->adapter->send($message, $this->recipients);
    }
}
