<?php

namespace YiiLogEx\Adapter;

use Yii;
use yii\helpers\FileHelper;
use YiiLogEx\Exception\Exception;

class SmsV1 implements AdapterInterface
{
    /**
     * @var int
     */
    protected $messageLimit;

    /**
     * @param string $phone
     * @return bool
     */
    protected function validatePhone($phone)
    {
        // TODO: phone-validator
        return true;
    }

    /**
     * @param string $phone
     * @return string
     */
    protected function filterPhone($phone)
    {
        // TODO: phone filter
        return $phone;
    }

    /**
     * @param string $message
     * @return string
     */
    protected function filterMessage($message)
    {
        // TODO: filter
        return $message;
    }

    /**
     * @param string $message
     * @param string $phone
     * @throws \yii\base\Exception
     */
    protected function sendToRecipient($message, $phone)
    {
        $logPath = sprintf('%s/logs/sms%s.log', Yii::$app->getRuntimePath(). $phone);
        FileHelper::createDirectory(dirname($logPath));
        @file_put_contents($logPath, $message, FILE_APPEND);
    }

    /**
     * @param array $options
     * @throws Exception
     */
    public function setOptions($options = [])
    {
        if (!is_array($options)) {
            throw new Exception();
        }
        if (array_key_exists('message_limit', $options)) {
            $this->messageLimit = $options['message_limit'];
        }
    }

    /**
     * @param string $message
     * @param string|array $recipients
     * @throws Exception
     */
    public function send($message, $recipients)
    {
        if (!is_scalar($message)) {
            throw new Exception();
        }
        if (!is_array($recipients)) {
            $recipients = [$recipients];
        }

        $message = $this->filterMessage($message);
        foreach ($recipients as $phone) {
            $phone = $this->filterPhone($phone);
            if (!$this->validatePhone($phone)) {
                throw new Exception();
            }
            $this->sendToRecipient($message, $phone);
        }
    }
}