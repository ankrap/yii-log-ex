<?php

namespace YiiLogEx\Adapter;

interface AdapterInterface
{
    /**
     * @param string $message
     * @param array|string $recipients
     * @return void
     */
    public function send($message, $recipients);

    /**
     * @param array $options
     * @return void
     */
    public function setOptions($options = []);
    
}